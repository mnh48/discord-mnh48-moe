# The MNH48 Discord

Backup of the raw files used in setting up The MNH48 Discord server.

This might also be useful for other people who want to setup their Discord server in the same way as I did.


## License

For all text files with the file extension `.txt` in this repository, they are released under the MIT license.

For all images and project files, please see the license file in respective directories.

